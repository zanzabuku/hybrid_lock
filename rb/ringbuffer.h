#ifndef RING_BUFFER
#define RING_BUFFER

// Provides ring (circular) buffer container with the concurrent access. 
//
//
// Common description
// ----------------------------------------------------------------------------
// The circular buffer is backed in memory by the linear array of a fixed size.
// This size must be a power of 2 (except '0' and '1') and equal to the template 
// parameter 'CAPACITY'. Two indecees 'head' and 'tail' mark the first and 
// the last elements in the buffer respectively. The current 'tail' index always
// points to the first empty place in the buffer available for new insertion or 
// equal to 'head'. The current 'head' index always points to the first available 
// element in the buffer of equal to 'tail'.
//
// ---------------------------------------------------------
// |   | * | * | * | * | * | * | * |   |   |   |   |   |   |
// ---------------------------------------------------------
//       'head                       'tail
//
// There are two main functions that manipulate elements in the ring buffer: 
// 'push' and 'pop'. First adds new element to the 'tail' and the second one
// returns first available element and removes it from the buffer.
// When the 'head' or 'tail' index grows up to 'size - 1' value it is
// truncated to '0' and grows again, etc.
// If the buffer is empty, 'head' == 'tail'.
//
//
// Concurrency properties
// ----------------------------------------------------------------------------
// Single consumer ('pop') and single producer ('push') are allowed 
// to be working concurrently. Any (unlimited) number of threads can call 
// 'size' and 'capacity' member functions in parallel.
//
// NOTE!!! That 'creator functions' and copy operators are not allowed 
// to be called in parallel with any other member functions.
//
// 'RingBuffer' container uses atomic variables to guarantee data consistance and proide
// fast interface to the main data acess operations and a mutex to interrupt a thead 
// execution. This component is not a lock-free and not a wait-free.

#include <array>
#include <atomic>
#include <mutex>

template <typename DT, std::size_t CAPACITY> class RingBuffer {
	// Implement ring buffer container. For more details see the entire component description.

private:
	// Private types
	using data_type = DT;
		// Alias for the container data type.

	using condition = std::unique_ptr<std::condition_variable>;
	    // Alias for the unique pointer on a 'std::condition_variable>'.

	using mutex = std::unique_ptr<std::mutex>;
	    // Alias for the unique pointer on a 'std::mutex'.

private:
	// Data
	std::atomic<std::size_t> d_head;                   // atomic head index
	std::atomic<std::size_t> d_size;                   // accumulated elements count
	std::atomic<std::size_t> d_tail;                   // atomic tail index

	condition                d_cond;                   // condition variable
	mutex                    d_mutex;                  // mutex
	std::atomic<bool>        d_pop_locked;             // popping predicate
	std::atomic<bool>        d_push_locked;            // pushing predicate

	std::array<DT, CAPACITY> d_buffer;                 // elements buffer
	std::size_t              d_cut_off;                // cut off for the spinlock wait counter
	const size_t             d_mask{ CAPACITY - 1 };   // index mask
	

public:
	// Public class methods
	void push(const data_type &value) noexcept;
		// Store the specified 'value' in the 'buffer' at the 'tail' position 
		// and increment 'tail' index by 1. If the 'buffer' has no empty space
		// wait in a cycle in the spin-lock manner till the space is freed 
		// or till the 'cut_off' limit is acheeved. If the 'cut_off' limit is acheeved,
		// block on the 'event'.
		// TODO: investigate universal reference.

	data_type pop() noexcept;
		// Return the first available value in 'buffer' specified by the 'head'
		// index and increment 'head' by 1. If the 'buffer' is empty (i.e. 'tail' = 'head')
		// wait in a cycle in the spin-lock manner till the new value is added 
		// or till the 'cut_off' limit is acheeved. If the 'cut_off' limit is acheeved,
		// block on the 'event'.

		// TODO: investigate universal reference.

	bool try_push(const data_type &value) noexcept;

	bool try_pop(data_type &value) noexcept;

	size_t size() const noexcept;
		// Return the number of the elements in the 'buffer'.

	size_t capacity() const noexcept;
		// Return the 'buffer' capacity.

	bool is_empty() const noexcept;
	    // Return 'true' if the buffer is empty.

	bool is_full() const noexcept;
		// Return 'true' if the buffer is full.


	// Creators
	explicit RingBuffer< DT, CAPACITY>() noexcept;
		// Create this object. Default constructor.

	explicit RingBuffer< DT, CAPACITY>(std::size_t cut_off) noexcept;
		// Create this object.

	explicit RingBuffer< DT, CAPACITY>(const RingBuffer& other) noexcept;
		// Create this object using the specified 'other' object.

	RingBuffer< DT, CAPACITY>(RingBuffer&& other) noexcept;
		// Create this object using the specified 'other' object.


	// Operators
	RingBuffer< DT, CAPACITY> & operator=(RingBuffer< DT, CAPACITY> &other);
		// Assign the specified 'other' to this object. This operation is not
		// thread-safe.

private:
	// Type traits
	static_assert(((CAPACITY - 1) & CAPACITY) == 0, "Ring bffer capacity must be power of '2', for example '1024'");
	// Force to use the capacity equal to a power of '2'.

	static_assert(CAPACITY != 0, "Ring bffer capacity must not be equal to '0'");
	// Prevent the capacity to be equal to '0'.

	static_assert(CAPACITY != 1, "Ring bffer capacity must not be equal to '1'");
	// Prevent the capacity to be equal to '1'.
};

template <typename DT, std::size_t CAPACITY>
inline
RingBuffer<DT, CAPACITY>::RingBuffer() noexcept
	: d_head(0)
	, d_size(0)
	, d_tail(0)
	, d_cond(new std::condition_variable())
	, d_mutex(new std::mutex())
	, d_pop_locked(false)
	, d_push_locked(false)
	, d_cut_off(10u)
{
}

template <typename DT, std::size_t CAPACITY>
inline
RingBuffer<DT, CAPACITY>::RingBuffer(std::size_t _cut_off) noexcept
	: d_head(0)
	, d_size(0)
	, d_tail(0)
	, d_cond(new std::condition_variable())
	, d_mutex(new std::mutex())
	, d_pop_locked(false)
	, d_push_locked(false)
	, d_cut_off(_cut_off)
{
}

template <typename DT, std::size_t CAPACITY>
inline
RingBuffer< DT, CAPACITY>::RingBuffer(const RingBuffer& other) noexcept
	: d_head(other.d_head.load())
	, d_size(other.d_size.load())
	, d_tail(other.d_tail.load())
	, d_cond(new std::condition_variable())
	, d_mutex(new std::mutex())
	, d_pop_locked(false)
	, d_push_locked(false)
	, d_buffer(other.d_buffer)
	, d_cut_off(other.d_cut_off)
{
}

template <typename DT, std::size_t CAPACITY>
inline
RingBuffer< DT, CAPACITY>::RingBuffer(RingBuffer&& other) noexcept
	: d_head(other.d_head.load())
	, d_size(other.d_size.load())
	, d_tail(other.d_tail.load())
	, d_cond(std::move(other.d_cond))
	, d_mutex(std::move(other.d_mutex))
	, d_pop_locked(other.d_pop_locked.load())
	, d_push_locked(other.d_push_locked.load())
	, d_buffer(std::move(other.d_buffer))
	, d_cut_off(other.d_cut_off)
{
}

template <typename DT, std::size_t CAPACITY>
inline
void RingBuffer<DT, CAPACITY>::push(const data_type &value) noexcept
{
	std::size_t c{ 0 };
	auto new_tail = (d_tail + 1) & d_mask;

	while (is_full()) {
		if (c++ == d_cut_off) {
			d_push_locked = true;
			if (!is_full()) {
				d_push_locked = false;
				break;
			}
			std::unique_lock<std::mutex> lock{ *d_mutex };
			d_cond->wait(lock, [this] { return !d_push_locked; });
		}
	}

	d_buffer[d_tail] = value;
	d_tail = new_tail;
	++d_size;
	if (d_pop_locked) {
		d_pop_locked = false;
		std::lock_guard<std::mutex> guard{ *d_mutex };
		d_cond->notify_all();
	} 
}

template <typename DT, std::size_t CAPACITY>
inline
DT RingBuffer<DT, CAPACITY>::pop() noexcept
{
	std::size_t c{ 0 };
	auto new_head = (d_head + 1) & d_mask;

	while (is_empty()) {
		if (c++ == d_cut_off) {
			d_pop_locked = true;
			if (!is_empty()) {
				d_pop_locked = false;
				break;
			}
			std::unique_lock<std::mutex> lock{ *d_mutex };
			d_cond->wait(lock, [this] { return d_size != 0; });
		}
	}

	data_type ret_value = std::move(d_buffer[d_head]);
	d_head = new_head;
	--d_size;
	if (d_push_locked) {
		d_push_locked = false;
		std::lock_guard<std::mutex> guard{ *d_mutex };
		d_cond->notify_all();
	}
	return ret_value;
}

template <typename DT, std::size_t CAPACITY>
inline
size_t RingBuffer<DT, CAPACITY>::size() const noexcept
{
	return d_size;
}

template <typename DT, std::size_t CAPACITY>
inline
size_t RingBuffer<DT, CAPACITY>::capacity() const noexcept
{
	return d_buffer.size();
}

template <typename DT, std::size_t CAPACITY>
inline
bool RingBuffer<DT, CAPACITY>::is_empty() const noexcept
{
	return d_size == 0;
}

template <typename DT, std::size_t CAPACITY>
inline
bool RingBuffer<DT, CAPACITY>::is_full() const noexcept
{
	return d_size == CAPACITY;
}

template <typename DT, std::size_t CAPACITY>
inline
RingBuffer< DT, CAPACITY> & RingBuffer<DT, CAPACITY>::operator=(RingBuffer< DT, CAPACITY> &other)
{
	this->d_buffer = other.d_buffer;
	this->d_cut_off = other.d_cut_off;
	this->d_head = other.d_head;
	this->d_tail = other.d_tail;
	this->d_size = other.d_size;
}

#endif