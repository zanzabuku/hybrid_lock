#include <ringbuffer.h>

#include <tu_barrier.h>

#include <chrono>
#include <random>

#include <gtest/gtest.h>

// Contains 'ringbuffer' unit tests.

class ringbuffer : public ::testing::Test {};

TEST_F(ringbuffer, breathing) 
{
	// Breathing test for the 'ringbuffer' component.
	{
		RingBuffer<int, 4> a;
		RingBuffer<int, 8> b;
	}
}

TEST_F(ringbuffer, value_semantic_type) 
{
	// Creators and assignment operator test
	{
		const std::size_t BUFFER_SIZE = 4;

		RingBuffer<int, BUFFER_SIZE> a;
		for (int i = 0; i < BUFFER_SIZE; ++i) {
			a.push(i);
		}

		RingBuffer<int, BUFFER_SIZE> b;
		for (int i = 0; i < BUFFER_SIZE; ++i) {
			b.push(i);
		}

		RingBuffer<int, BUFFER_SIZE> c(a);
		RingBuffer<int, BUFFER_SIZE> d(b);

		for (int i = 0; i < BUFFER_SIZE; ++i) {
			EXPECT_EQ(c.pop(), d.pop());
		}

		EXPECT_EQ(c.capacity(), d.capacity());
		EXPECT_EQ(c.size(), d.size());
	}

	{
		const std::size_t BUFFER_SIZE = 4;

		RingBuffer<int, BUFFER_SIZE> a;
		for (int i = 0; i < BUFFER_SIZE; ++i) {
			a.push(i);
		}

		RingBuffer<int, BUFFER_SIZE> b;
		for (int i = 0; i < BUFFER_SIZE; ++i) {
			b.push(i);
		}

		RingBuffer<int, BUFFER_SIZE> c(a);
		RingBuffer<int, BUFFER_SIZE> d(std::move(b));

		for (int i = 0; i < BUFFER_SIZE; ++i) {
			EXPECT_EQ(c.pop(), d.pop());
		}

		EXPECT_EQ(c.capacity(), d.capacity());
		EXPECT_EQ(c.size(), d.size());
	}
}

TEST_F(ringbuffer, atomicy_correctness)
{
	// Test atomicy correctness.
	// 
	// Test plan.
	// 1. Bulk data transfer: singular test with the BUFFER_SIZE = 2. 
	//    Check for the correctness of writing and reading.
	// 2. Bulk data transfer: average buffer size. Check for the correctness
	// of writing and reading.
	// 3. 'Pulsing' reading and writing.
	// 4. The 'size', 'empty' and 'full' correctness after multiple 
	//    concurrent reads and writes.

	{
		const std::size_t BUFFER_SIZE = 2;
		const std::size_t BULK_DATA_SIZE = 1000000;

		std::mutex mutex;
		Barrier barier(2);

		RingBuffer<std::size_t, BUFFER_SIZE> a;

		std::thread consumer{
			[&]() {
				barier.wait();
				int previous_value = -1;
				for (std::size_t i = 0; i < BULK_DATA_SIZE; ++i) {
					auto value = a.pop();
					EXPECT_EQ(value, ++previous_value);
				}		
		} };

		std::thread producer{
			[&]() {
			barier.wait();
			for (std::size_t i = 0; i < BULK_DATA_SIZE; ++i) {
				a.push(i);
			}
		} };

		consumer.join();
		producer.join();
	}

	{
		const std::size_t BUFFER_SIZE = 2048;
		const std::size_t BULK_DATA_SIZE = 1000000;

		std::mutex mutex;
		Barrier barier(2);

		RingBuffer<std::size_t, BUFFER_SIZE> a;

		std::thread consumer{
			[&]() {
			barier.wait();
			int previous_value = -1;
			for (std::size_t i = 0; i < BULK_DATA_SIZE; ++i) {
				auto value = a.pop();
				EXPECT_EQ(value, ++previous_value);
			}
		} };

		std::thread producer{
			[&]() {
			barier.wait();
			for (std::size_t i = 0; i < BULK_DATA_SIZE; ++i) {
				a.push(i);
			}
		} };

		consumer.join();
		producer.join();
	}

	{
		const std::size_t BUFFER_SIZE = 2048;
		const std::size_t BULK_DATA_SIZE = 100000;

		std::mutex mutex;
		Barrier barier(2);

		RingBuffer<int, BUFFER_SIZE> a;

		std::thread consumer{
			[&]() {
			barier.wait();

			std::random_device rd;
			std::mt19937 mt(rd());
			std::uniform_int_distribution<std::size_t> block_dist(1, 100);
			std::uniform_int_distribution<std::size_t> time_interval_dist(1, 10);

			int previous_value = -1;

			std::size_t total_read_count = 0;
			while (true) {
				auto elements_to_add = block_dist(mt);
				std::size_t read_count_in_block = 0;
				while (total_read_count < BULK_DATA_SIZE && read_count_in_block < elements_to_add) {
					auto value = a.pop();
					EXPECT_EQ(value, ++previous_value);

					++total_read_count;
					++read_count_in_block;
				}
				if (total_read_count < BULK_DATA_SIZE) {
					using namespace std::chrono_literals;
					std::size_t sleep_interval = time_interval_dist(mt);
					std::this_thread::sleep_for(std::chrono::duration<std::size_t, std::milli>(sleep_interval));
				}
				else {
					break;
				}
			}
		} };

		std::thread producer{
			[&]() {
			barier.wait();

			std::random_device rd;
			std::mt19937 mt(rd());
			std::uniform_int_distribution<std::size_t> block_dist(1, 100);
			std::uniform_int_distribution<std::size_t> time_interval_dist(1, 10);

			std::size_t total_read_count = 0;
			while (true) {
				auto elements_to_add = block_dist(mt);
				std::size_t read_count_in_block = 0;
				while (total_read_count < BULK_DATA_SIZE && read_count_in_block < elements_to_add) {
					a.push(total_read_count++);
					++read_count_in_block;
				}
				if (total_read_count < BULK_DATA_SIZE) {
					using namespace std::chrono_literals;
					std::size_t sleep_interval = time_interval_dist(mt);
					std::this_thread::sleep_for(std::chrono::duration<std::size_t, std::milli>(sleep_interval));
				}
				else {
					break;
				}
			}
		} };

		consumer.join();
		producer.join();
	}
}

TEST_F(ringbuffer, large_objects)
{
	// Test work with large objects.
}

TEST_F(ringbuffer, allocator_aware)
{
	// Test allocator-awareness.
}