#ifndef TIME_UTIL
#define TIME_UTIL

#include <cstddef>

// Provides utilities the help measure time intervals.

struct TimeUtil {
	// Provides for a suite of time measurement utilities.

private:
	static const std::size_t MUTEX_TEST_COUNT{ 10 };
	static const std::size_t SPINLOCK_TEST_COUNT{ 1000 };

public:
	static std::size_t measure_spin_mutex_ratio();
	    // Measure ratio between execution of a single 'lock -> unlock'
	    // cicle for a mutex and a spinlock. Rely on the property of standard
	    // 'std::clock()' clock that the time being measured is 'process-local'
	    // and indicates rater CPU tacts then real time flow.
	    // In order to acheeve better accuracy both durations are measured in
		// cycle and then average value evaluated.
};

#endif