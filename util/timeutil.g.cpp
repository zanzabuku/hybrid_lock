#include <timeutil.h>

#include <iostream>

#include <gtest/gtest.h>


// Contains 'timeutil' unit tests.

class timeutil : public ::testing::Test {};

TEST_F(timeutil, breathing)
{
	// Breathing test for the 'timeutil' component.
	{
		std::cout << ">>> " << TimeUtil::measure_spin_mutex_ratio();
	}
}
