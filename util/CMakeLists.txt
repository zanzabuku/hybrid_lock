cmake_minimum_required(VERSION 3.10)

project (util)

include(CTest)

# Include directories
set(CMAKE_INCLUDE_CURRENT_DIR ON)

file(GLOB SRCS *.cpp)
file(GLOB TEST_SRCS *.g.cpp)
LIST(REMOVE_ITEM SRCS ${TEST_SRCS})

add_library(
	${PROJECT_NAME}	STATIC 
	${SRCS})

set(TEST_APP ${PROJECT_NAME}_tests)

add_executable(${TEST_APP} 
    ${TEST_SRCS})

target_link_libraries(${TEST_APP}
    gtest_main
	${PROJECT_NAME}
)

add_test(unit_test ${TEST_APP})

install(TARGETS ${TEST_APP} DESTINATION bin)