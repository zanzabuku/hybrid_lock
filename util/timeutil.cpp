
#include <timeutil.h>

#include <atomic>
#include <cmath>
#include <mutex>
#include <time.h>

std::size_t TimeUtil::measure_spin_mutex_ratio()
{

	clock_t start;
	clock_t stop;
	
	clock_t total_m = 0;
	std::mutex mutex;
	for (volatile auto i = 0; i < MUTEX_TEST_COUNT; ++i) {
		start = clock();
		mutex.lock();
		mutex.unlock();
		stop = clock();
		total_m += (stop - start);
	}

	double mutex_cycle_duration = total_m / (double)MUTEX_TEST_COUNT;

	clock_t total_s = 0;
	std::atomic<std::size_t> counter = 0;
	for (volatile auto i = 0; i < SPINLOCK_TEST_COUNT; ++i) {
		start = clock();
		while (counter != i) {
			auto tmp = counter.load();
			counter.compare_exchange_strong(tmp, i);
		}
		stop = clock();
		total_s += (stop - start);
	}

	double spinlock_cycle_duration = total_s / (double)SPINLOCK_TEST_COUNT;
	return static_cast<std::size_t>(
		std::lround(mutex_cycle_duration / spinlock_cycle_duration));
}


