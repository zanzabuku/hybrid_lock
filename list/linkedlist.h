#ifndef LINKED_LIST
#define LINKED_LIST

// Provides linked list container with the concurrent access.

#include <atomic>

template <typename DT>
class list {

private:
	// Private types
	using data_type = DT;
	// Alias for the container data type.

	using node_ptr = node< data_type >;
	// Alias for the pointer to the container data type.

	template < typename data_type >
	class node {
		node_ptr next;
		node_ptr prev;
		data_type data;
	};

private:
	// Data
	data_type_ptr head;
	data_type_ptr tail;

	std::size_t size;

public:
	// Public class methods
	void push_back(const data_type& data);

	data_type pop_front() noexcept;

	// Creators
	explicit list< DT >() noexcept;
	// Create this object. Default constructor.

	explicit list< DT >(std::size_t cut_off) noexcept;
	// Create this object.

	explicit list< DT >(const list& other) noexcept;
	// Create this object using the specified 'other' object.

	list< DT >(list&& other) noexcept;
	// Create this object using the specified 'other' object.


	// Operators
	list< DT > & operator=(list< DT > &other);
	// Assign the specified 'other' to this object. This operation is not
	// thread-safe.
};

template <typename DT>
inline
void list<DT>::push_back(const data_type& data)
{

}

#endif