#ifndef BARRIER
#define BARRIER

#include <mutex>

class Barrier {
private:
	using condition = std::unique_ptr<std::condition_variable>;

	using mutex = std::unique_ptr<std::mutex>;

private:
	// Data
	condition   d_cond;       // condition variable
	std::size_t d_count;      // release threads counter
	std::size_t d_generation; // release threads counter
	mutex       d_mutex;      // mutex
	std::size_t d_threshold;  // number of threads to block

public:
	// Class methods
	void wait();

	// Creators
	Barrier(size_t count);
};

#endif