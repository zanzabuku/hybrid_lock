
#include <tu_barrier.h>

#include <mutex>

void Barrier::wait()
{
	std::unique_lock<std::mutex> lock{ *d_mutex };
	if (--d_count == 0) {
		d_count = d_threshold;
		++d_generation;
		d_cond->notify_all();
		return;
	}

	auto generation = d_generation;
	while (generation == d_generation) {
		d_cond->wait(lock, [&, this] { 
			return generation != d_generation; 
		});
	}
}

// Creators
Barrier::Barrier(size_t count)
	: d_cond(new std::condition_variable())
	, d_count(count)
	, d_generation(0)
	, d_mutex(new std::mutex())
	, d_threshold(count)
	
{
}